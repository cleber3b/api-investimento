package com.br.apiinvestimento.models;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
@Table(name = "simulacoes")
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nome obrigatório")
    @NotBlank(message = "Nome obrigatório")
    private String nome;

    @Email(message = "Email Inválido")
    private String email;

    @DecimalMin(value = "10.0", message = "Valor do investimento precisa ser maior que R$10.00")
    @Digits(integer = 6, fraction = 2, message = "Valor da aplicação fora do padrão")
    private double valorAplicacao;

    @NotNull(message = "Informe pelo menos 1 mês")
    @Min(value = 1, message = "Informe pelo menos 1 um mês para simular seu investimento")
    private int quantidadeMeses;

    @ManyToOne
    private Investimento investimento;

    public Simulacao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicacao() {
        return valorAplicacao;
    }

    public void setValorAplicacao(double valorAplicacao) {
        this.valorAplicacao = valorAplicacao;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
