package com.br.apiinvestimento.services;

import com.br.apiinvestimento.models.Investimento;
import com.br.apiinvestimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento) {
        return investimentoRepository.save(investimento);

    }

    public Iterable<Investimento> retornarTodosInvestimentos() {
        return investimentoRepository.findAll();
    }

    public Investimento buscarInvestimentoPorId(int idInvestimento){

        Optional<Investimento> investimentoOptional = investimentoRepository.findById(idInvestimento);

        if(investimentoOptional.isPresent()){
            Investimento investimento = investimentoOptional.get();
            return investimento;
        }else {
            throw new RuntimeException("Investimento não foi encontrado");
        }
    }


}
