package com.br.apiinvestimento.services;

import com.br.apiinvestimento.DTOs.SimulacaoDTO;
import com.br.apiinvestimento.models.Investimento;
import com.br.apiinvestimento.models.Simulacao;
import com.br.apiinvestimento.repositories.InvestimentoRepository;
import com.br.apiinvestimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoRepository simulacaoRepository;


    public SimulacaoDTO salvarSimulacaoDTO(Simulacao simulacao, int idInvestimento) {

        SimulacaoDTO retornoSimulacaoDTO = new SimulacaoDTO();
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(idInvestimento);

        if (investimentoOptional.isPresent()) {
            Investimento investimento = investimentoOptional.get();

            simulacao.setInvestimento(investimento);
            simulacaoRepository.save(simulacao);

            return retornoSimulacaoDTO = this.SimularInvestimento(simulacao.getValorAplicacao(), simulacao.getQuantidadeMeses(),investimento.getRendimentoAoMes());
        } else {
            throw new RuntimeException("Id do Investimento inválido");
        }
    }

    private SimulacaoDTO SimularInvestimento(double valorAplicacao, int quantidadeMeses, double taxaAoMes) {

        SimulacaoDTO retornoSimulacaoDTO = new SimulacaoDTO();
        retornoSimulacaoDTO.setRendimentoPorMes(taxaAoMes);

        double resultadoMontante = valorAplicacao * Math.pow (1 + (taxaAoMes/100), quantidadeMeses);

        BigDecimal montanteFormatado = new BigDecimal(resultadoMontante);
        montanteFormatado = montanteFormatado.setScale(2, RoundingMode.UP);
        retornoSimulacaoDTO.setMontante(montanteFormatado.doubleValue());

        return retornoSimulacaoDTO;
    }

    public Iterable<Simulacao> retornarTodasAsSimulacoes() {
        return simulacaoRepository.findAll();
    }
}
