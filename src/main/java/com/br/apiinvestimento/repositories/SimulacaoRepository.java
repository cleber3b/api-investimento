package com.br.apiinvestimento.repositories;

import com.br.apiinvestimento.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {

}
