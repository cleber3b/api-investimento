package com.br.apiinvestimento.repositories;

import com.br.apiinvestimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {

}
