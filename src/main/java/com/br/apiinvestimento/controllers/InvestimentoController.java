package com.br.apiinvestimento.controllers;

import com.br.apiinvestimento.DTOs.SimulacaoDTO;
import com.br.apiinvestimento.models.Investimento;
import com.br.apiinvestimento.models.Simulacao;
import com.br.apiinvestimento.services.InvestimentoService;
import com.br.apiinvestimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento casdtrarInvestimento(@RequestBody @Valid Investimento investimento){
        return investimentoService.salvarInvestimento(investimento);
    }

    @GetMapping
    public Iterable<Investimento> retornaTodosInvestimentos(){
        return investimentoService.retornarTodosInvestimentos();
    }

    @GetMapping("/{id}")
    public Investimento retornaTodosInvestimentos(@RequestBody @PathVariable int id){
        return investimentoService.buscarInvestimentoPorId(id);
    }

    @PostMapping("/{idInvestimento}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public SimulacaoDTO simularInvestimento(@RequestBody @Valid Simulacao simulacao, @PathVariable(name = "idInvestimento") int idInvestimento){
        return simulacaoService.salvarSimulacaoDTO(simulacao, idInvestimento);

    }



}
