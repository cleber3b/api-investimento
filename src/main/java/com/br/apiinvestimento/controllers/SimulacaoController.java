package com.br.apiinvestimento.controllers;

import com.br.apiinvestimento.models.Simulacao;
import com.br.apiinvestimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> retornaTodosInvestimentos(){
        return simulacaoService.retornarTodasAsSimulacoes();
    }
}
